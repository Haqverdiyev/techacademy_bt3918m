# -*- coding: utf-8 -*-

# 1. For döngüsü vasitəsilə "Salam Dünya" mətnini 10 dəfə çap et
for i in range(10):
    print("1. Salam Dünya")

# 2. "Salam Dünya" mətninin bütün simvollarını bir bir ekranda çap et
myStr = "Salam Dünya"
for i in myStr:
    print("2.", i)

# 3. For döngüsü vasitəsilə sətri tərsinə (reversed) yazın
myStr = "Salam Dünya"
print("3.", end="")
for i in range(len(myStr)-1, -1, -1):
    print(myStr[i], end="")
    i -= 1
print("")

# 4. For döngüsü vasitəsilə şam ağacı çəkin
limit = 20
print("4. Şam ağacı")
for i in range(limit):
    print(" " * (limit - i - 1), "*" * (i * 2 + 1))
    i += 1

# 5. "Salam Dünya" mətninin bütün simvollarını çap edin və çap olunan simvolun indeksini qarşısında yazın
myStr = "Salam Dünya"
for index, simvol in enumerate(myStr):
    print(f"5. Index: {index}, Simvol: {simvol}")

# 6. 1-100 aralığında yalnızca 3-ə bölünən ədədləri çap edin.
print("6. Üçə bölünən rəqəmlər:", end=" ")
for i in range(0, 101, 3):
    print(i, end=" ")
print("")

# 7. 1-100 aralığında yalnızca 5-ə bölünən ədədləri çap edin, continue əmri istifadə etmək şərtilə.
print("7. Beşə bölünən rəqəmlər:", end=" ")
for i in range(101):
    if i % 5 > 0:
        continue
    print(i, end=" ")
print("")

# 8. 100-dən 1-ə qədər olan bütün tək rəqəmləri çap edin
print("8. 100-dən 1-ə qədər olan bütün tək rəqəmlər:", end=" ")
for i in range(99, -1, -2):
    print(i, end=" ")
print("")

# 9. 100-dən 1-ə qədər olan bütün tək rəqəmləri çap edin. continue əmri istifadə etmək şərtilə
print("9. 100-dən 1-ə qədər olan bütün tək rəqəmlər:", end=" ")
for i in range(100, -1, -1):
    if i % 2 == 0:
        continue
    print(i, end=" ")
print("")

# 10. break istifadə etməklə sətrin içərisində m simvolunun indeksini ekranda çap edin
myStr = "Salam Dünya"
tapilmaliHerf = "m"
for index, character in enumerate(myStr):
    if character == tapilmaliHerf:
        print(f"10. \"{myStr}\" mətnində \"{tapilmaliHerf}\" hərfinin indeksi: {index}")
        break

# 11. continue istifadə etməklə sətrin içərisində y simvolunun indeksini ekranda çap edin
myStr = "Salam Dünya"
tapilmaliHerf = "y"
for index, character in enumerate(myStr):
    if character != tapilmaliHerf:
        continue
    print(f"11. \"{myStr}\" mətnində \"{tapilmaliHerf}\" hərfinin indeksi: {index}")