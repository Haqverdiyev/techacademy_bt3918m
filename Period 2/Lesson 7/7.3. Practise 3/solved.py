# verilən siyahıların birində şəxs adları, digərində soyadları, sonuncuda isə doğulduğu illər qeyd edilmişdir. Bu ad, soyad və illəri yanaşı print edin
# =========================================================
names = ["Fərid", "Rza", "Xəyal", "Qabil", "Elgiz", "Cavidan", "Elşad", "Şəhriyar"]
surnames = ["Mahmudov", "Hüseynov", "Yediyarov", "Orucov", "İbrahimli", "Hüseynov", "Ağayev", "Rzayev"]
ages = ["25", "17", "22", "23", "29", "22", "31", "29"]

for name, surname, age in zip(names, surnames, ages):
    print(name, surname, age)
# =========================================================

# input() vasitəsi ilə istifadəçidən növbə ilə müxtəlif şəxs adları və soyadları daxil etməsini tələb edin. Əgər istifadəçi quit sözü yazarsa onda daxiletməni dayandırmaq və bütün şəxslərin ad və soyadlarını print etmək lazımdır. Əgər ad və soyad təkrarlanarsa onda o şəxsi daxil etməmək və istifadəçiyə bu barədə xəbərdarlıq etmək lazımdır
# =========================================================
# bütün daxil ediləcək şəxslər bu siyahıda saxlanacaq
persons = []

# köməkçi siyahıdır. Daxil edilən hər bir şəxsin adını və soyadını növbə ilə özündə saxlayacaq
person = []

while True:
    # əgər köməkçi person siyahısının ölçüsü sıfra bərabərdirsə onda istifadəçidən şəxsin adını daxil etməsini tələb edirik
    if len(person) == 0:
        # istifadəçidən şəxsin adını daxil etməsini tələb edərkən ona indiyədək daxil etdiyi şəxslərin sayını len(persons) vasitəsilə göstəririk və növbəti şəxsin adını yazmasını tələb edirik
        value = input(f"{len(persons)} şəxs daxil edilib. Növbəti şəxsin adını daxil edin. Əgər sona çatdırmaq istəyirsinizsə onda \"quit\" yazın:")
        # əgər daxil edilən məlumat istifadəçinin adı yox quit sözüdürsə onda döngünü dayandırıb və olduğumuz yerdən onu tərk edirik
        if value == "quit":
            break
        # əks halda istifadəçinin adını person siyahısına daxil edirik. Bu sıfırıncı element olacaq
        person.append(value)
    # əgər köməkçi person dəyişənin elementlərinin sayı 1-ə bərabərdirsə deməli şəxsin adı artıq daxil edilib. Ona görə də bu dəfə şəxsin soyadını soruşuruq
    elif len(person) == 1:   
        # istifadəçidən şəxsin soyadını daxil etməsini tələb edərkən ona indiyədək daxil etdiyi şəxslərin sayını len(persons) vasitəsilə göstəririk, daha sonra istifadəçidən hal-hazırda daxil etmək istədiyi şəxsin adını göstərərək soyadını da daxil etməsini tələb edirik  
        value = input(f"{len(persons)} şəxs daxil edilib. {person[0]} adlı şəxsin soyadını da daxil edin. Əgər sona çatdırmaq istəyirsinizsə onda \"quit\" yazın:")
        # əgər daxil edilən məlumat istifadəçinin adı yox quit sözüdürsə onda döngünü dayandırıb və olduğumuz yerdən onu tərk edirik
        if value == "quit":
            break
        # soyad daxil edildikdən sonra onu da köməkçi person siyahısına əlavə edirik. Bu həmən siyahıda ikinci element olacaq və nəticədə əlimizdə iki elementli person siyahısı olmuş olacaq
        person.append(value)
        
        # köməkçi person siyahısını əsas persons siyahısına əlavə etməmişdən əvvəl onun həmən siyahıda əvvəlcədən olub olmamasını yoxlayırıq
        if person in persons:
            # əgər daxil edilən şəxs artıq əsas siyahıda varsa onda istifadəçiyə bu barədə mesaj çıxarırıq
            print(f"{person[0]} {person[1]} adlı şəxs artıq daxil edilib. Zəhmət olmasa fərqli şəxsi daxil edin")
            # daha sonra köməkçi person siyahısını sıfırlayırıq
            person = []
            # və döngünü olduğumuz yerdən kəsib növbəti iterasiyaya keçməsinə məcbur edirik
            continue
        
        # əgər daxil edilən şəxs əsas siyahıda yoxdursa onda bu şəxsi əsas siyahıya daxil edirik
        persons.append(person)
        # köməkçi person siyahısını sıfırlayırıq və döngünün növbəti iterasiyasına keçirik
        person = []

# döngü dayandırıldıqdan sonra əsas siyahıda olan bütün şəxsləri print edirik
print(persons)
# =========================================================