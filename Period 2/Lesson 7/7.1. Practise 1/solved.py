# yalnızca rəqəmlərdən ibarət 20 elementi olan siyahı yaradın. Rəqəmlər təsadüfi olmalıdır (random moldulundan istifadə edin) və 1000-i keçməməlidir.
# ==============================================
from random import randint
numlist = []

for i in range(20):
    numlist.append(randint(1, 1000))

print("numlist is", numlist)
# ==============================================

# 3x4x6 üç ölçülü və bütün elementləri yalnızca * işarəsindən ibarət olan list yaradın
# ==============================================
list3d = [[[[["*"] * 6] * 4] * 3]]
print(list3d)
# ==============================================

# yaradılan siyahının bütün elementlərinin cəmini tapın
# ==============================================
sum = 0
for i in numlist:
    sum += i

print("Sum of elements in numlist is", sum)
# ==============================================

# yaradılan siyahının bütün elementlərinin hasilini tapın
# ==============================================
multiplies = 1
for i in numlist:
    multiplies *= i

print("Multipy of all elements in numlist is", multiplies)
# ==============================================

# yaradılan siyahının elementləri içərisindən yalnızca cüt ədəd olanları print edin
# ==============================================
for i in numlist:
    if not i % 2:
        print(i, end=", ")
# ==============================================

# yaradılan siyahının ən böyük və ən kiçik elementini tapın
# ==============================================
min = numlist[0]
max = 0
for i in numlist:
    if min > i:
        min = i
    
    if max < i:
        max = i

print("\nminimum in numlist is", min)
print("maximum in numlist is", max)
# ==============================================

# verilmiş iki siyahıda ən azı bir cüt eyni elementlər varsa onda buna uyğun bildiriş mətni print edin, əks təqdirdə oxşar elementlərin olmadığını bildirin
# ==============================================
list1 = [1,2,3,5,7]
list2 = [8,9,10,3,12]

# ən uzun olan siyahını tapırıq və onu yeni dəyişənə mənimsədirik
largestlist = list1 if len(list1) > len(list2) else list2
# əz qısa olan siyahını tapırıq və onu yeni dəyişənə mənimsədirik
smallestlist = list1 if len(list1) < len(list2) else list2
# köməkçi result adlı dəyişən yaradırıq və default dəyərini False edirik. Siyahılarda oxşar element tapdıqda bu dəyişənin dəyərini True edəcəyik
result = False

# ən uzun siyahını sadalayırıq
for i in largestlist:
    # əgər i elementi digər siyahıda varsa onda result dəyişənini True edirik və döngünü dayandıraraq tərk edirik
    if i in smallestlist:
        result = True
        break

if result:
    print("Hər iki siyahıda oxşar elemenetlər var")
else:
    print("Siyahılarda oxşar elementlər tapılmadı")
# ==============================================

# verilmiş iki siyahıda oxşar elementlərin sayını print edin
# ==============================================
list1 = [1,2,3,5,7,12,8,5]
list2 = [8,9,10,3,12]

# ən uzun olan siyahını tapırıq və onu yeni dəyişənə mənimsədirik
largestlist = list1 if len(list1) > len(list2) else list2
# əz qısa olan siyahını tapırıq və onu yeni dəyişənə mənimsədirik
smallestlist = list1 if len(list1) < len(list2) else list2
# köməkçi count adlı dəyişən yaradırıq və default dəyərini 0 edirik. Siyahılarda hər dəfə oxşar element tapdıqda bu dəyişənin dəyərinin üzərinə 1 əlavə edəcəyik
count = 0

# ən uzun siyahını sadalayırıq
for i in largestlist:
    # əgər i elementi digər siyahıda varsa onda count dəyişəninin dəyərinin üzərinə 1 əlavə edirik
    if i in smallestlist:
        count += 1
        continue

print(f"Hər iki siyahıda {count} ədəd oxşar element var")
# ==============================================