# list comprehension vasitəsi ilə uzunluğu 20 olan, elementlərinin dəyəri 1000-dən böyük olmayan və elementləri random rəqəmlər olan siyahı yaradın
# ==============================================

# ==============================================

# yaradılan siyahının elementləri içərisindən yalnızca tək ədəd olanlardan yeni siyahı yaradın və print edin. Lakin bunu bir sətirlik kodla edin
# ==============================================

# ==============================================

# yaradılan siyahının əsasında yeni siyahı yaradın, lakin yeni siyahının elementləri əvvəlki siyahının elementlərinin kvadratı olmalıdır, və əgər elementin kvadratı 6-ya bölünürsə o zaman yeni siyahıya daxil edilməlidir
# ==============================================

# ==============================================