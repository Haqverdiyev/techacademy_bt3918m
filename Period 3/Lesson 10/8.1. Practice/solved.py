#  boş və boş olmayan setlər yaradın
# ======================================================
x = set()
print(x)

n = set([0, 1, 2, 3, 4])
print(n)
# ======================================================

#  seti sadalayın və hər bir elementini print edin
# ======================================================
num_set = set([0, 1, 2, 3, 4, 5])
for n in num_set:
  print(n)
# ======================================================

#  setə add və update metodları ilə yeni element artırın
# ======================================================
color_set = set()
color_set.add("Red")
print(color_set)

color_set.update(["Blue", "Green"])
print(color_set)
# ======================================================

#  setdən son iki elementi silin
# ======================================================
num_set = set([0, 1, 3, 4, 5])
num_set.pop()
print(num_set)
num_set.pop()
print(num_set)
# ======================================================

#  setdən əgər varsa elementi silin
# ======================================================
num_set = set([0, 1, 2, 3, 4, 5])
num_set.discard(4)
print(num_set)
# ======================================================

#  bitwise operatorun köməkliyi ilə intersection metodunun gördüyü işi edin
# ======================================================
setx = set(["green", "blue"])
sety = set(["blue", "yellow"])
setz = setx & sety
print(setz)
# ======================================================

#  bitwise operatorun köməkliyi ilə union metodunun gördüyü işi edin
# ======================================================
setx = set(["green", "blue"])
sety = set(["blue", "yellow"])
seta = setx | sety
print(seta)
# ======================================================

#  bitwise və - operatorların köməkliyi ilə difference metodun gördüyü işi edin
# ======================================================
setx = set(["apple", "mango"])
sety = set(["mango", "orange"])
setz = setx & sety
print(setz)

setb = setx - setz
print(setb)
# ======================================================

#  bitwise operatorun köməkliyi ilə symmetric_difference metodun gördüyü işi edin
# ======================================================
setx = set(["apple", "mango"])
sety = set(["mango", "orange"])

setc = setx ^ sety
print(setc)
# ======================================================

#  müqayisə operatorların köməkliyi ilə issubset və issuperset metodların gördüyü işi edin
# ======================================================
setx = set(["apple", "mango"])
sety = set(["mango", "orange"])
setz = set(["mango"])
issubset = setx <= sety
print(issubset)
issuperset = setx >= sety
print(issuperset)
issubset = setz <= sety
print(issubset)
issuperset = sety >= setz
print(issuperset)
# ======================================================

#  mövcud seti shallow copy vasitəsilə kopyalayın
# ======================================================
setp = set(["Red", "Green"])
setr = setp.copy()
print(setr)
# ======================================================

#  mövcud seti kopyalayın və təmizləyin
# ======================================================
setp = set(["Red", "Green"])
setq = setp.copy()
print(setq)
setq.clear()
print(setq)
# ======================================================

#  setin içərisində maximum və minimumu tapın
# ======================================================
seta = set([5, 10, 3, 15, 2, 20])
print(max(seta))
print(min(seta))
# ======================================================

#  setin uzunluğunu tapın
# ======================================================
seta = set([5, 10, 3, 15, 2, 20])
print(len(seta))
# ======================================================