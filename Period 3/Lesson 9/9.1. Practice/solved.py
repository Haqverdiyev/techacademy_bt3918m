# İki fərqli üsulla boş tuple yaradın
# =====================================================
x = ()
print(x)
tuplex = tuple()
print(tuplex)
# =====================================================

# Müxtəlif tipdə elementləri olan tuple yaradın
# =====================================================
tuplex = ("tuple", False, 3.2, 1)
print(tuplex)
# =====================================================

# Rəqəmlərdən ibarət olan tuple yaradın və bir elementini print edin
# =====================================================
tuplex = 5, 10, 15, 20, 25
print(tuplex)
print(tuplex[3])
# =====================================================


# korteji müxtəlif dəyişənlərə unpack edin
# =====================================================
tuplex = 4, 8, 3 
print(tuplex)
n1, n2, n3 = tuplex
print(n1, n2, n3) 
n1, n2, n3, n4 = tuplex 
# =====================================================

# Kortejə element artırın
# =====================================================
tuplex = (4, 6, 2, 8, 3, 1) 
print(tuplex)
#kortejlər immutable tiplərdir, ona görə də biz kortejə yeni element artıra bilmərik.
#Lakin merge üsulundan istifadə edərək yeni elementlə birləşdiririk və yeni tuple alırıq
tuplex = tuplex + (9,)
print(tuplex)
# xüsusi indeksə əlavə edirik
tuplex = tuplex[:5] + (15, 20, 25) + tuplex[:5]
print(tuplex)
# korteji siyahıya çeviririk
listx = list(tuplex) 
# yeni element artırırıq və yenidən kortejə çeviririk
listx.append(30)
tuplex = tuple(listx)
print(tuplex)
# =====================================================

# Korteji string-ə çevirin
# =====================================================
tup = ('T', 'e', 'c', 'h', 'A', 'c', 'a', 'd', 'e', 'm', 'y')
str =  ''.join(tup)
print(str)
# =====================================================

# Kortejin 4-cü və sondan 4-cü elementini print edin
# =====================================================
tuplex = ('T', 'e', 'c', 'h', 'A', 'c', 'a', 'd', 'e', 'm', 'y')
print(tuplex)
item = tuplex[3]
print(item)
item1 = tuplex[-4]
print(item1)
# =====================================================

# Kortejdə təkrarlanan 4 elementin sayını tapın
# =====================================================
tuplex = 2, 4, 5, 6, 2, 3, 4, 4, 7 
print(tuplex)
count = tuplex.count(4)
print(count)
# =====================================================

# Elementin kortejdə olub olmadığını yoxlayın və print edin
# =====================================================
tuplex = ('T', 'e', 'c', 'h', 'A', 'c', 'a', 'd', 'e', 'm', 'y')
print("r" in tuplex)
print(5 in tuplex)
# =====================================================

# Siyahını kortejə çevirin
# =====================================================
listx = [5, 10, 7, 4, 15, 3]
print(listx)
tuplex = tuple(listx)
print(tuplex)
# =====================================================

# Kortejdən elementi silin
# =====================================================
tuplex = ('T', 'e', 'c', 'h', 'A', 'c', 'a', 'd', 'e', 'm', 'y')
print(tuplex)
tuplex = tuplex[:2] + tuplex[3:]
print(tuplex)
listx = list(tuplex)
listx.remove("c")
tuplex = tuple(listx)
print(tuplex)
# =====================================================

# Korteji slicing üsulu ilə print edin
# =====================================================
tuplex = (2, 4, 3, 5, 4, 6, 7, 8, 6, 1)
# 1. kortejin indeksi 3 olan elementindən indeksi 4 olan elementinə qədər print edin
slice = tuplex[3:5]
print(slice)
# 2. kortejin əvvəlindən indeksi 5 olan elementə qədər print edin
slice = tuplex[:6]
print(slice)
# 3. kortejin 6-cı elementindən sona qədər print edin
slice = tuplex[5:]
print(slice)
# 4. korteji tərsinə çevirin
slice = tuplex[::-1]
print(slice)
# 5. kortejin sondan 6-cı elementindən sondan 3-cü elementinə qədər print edin
slice = tuplex[-6:-2]
print(slice)
# 6. Sətirdən yeni kortej düzəldin
tuplex = tuple("TECH ACADEMY")
# 7. yeni kortejin hər 2-ci elementini print edin
slice = tuplex[::2]
print(slice)
# =====================================================

# kortejdə elementin indeksini tapın
# =====================================================
tuplex = tuple("TECH ACADEMY")
print(tuplex)
# 1. D hərfinin indeksini tapın
index = tuplex.index("D")
print(index)
# 2. ilk 6 simvolun içində A hərfinin indeksini tapın
index = tuplex.index("A", 5)
print(index)
# 3. 4-cü elementən 8-ci elementə qədər olan hissədə C hərfinin indeksini tapın
index = tuplex.index("C", 3, 8)
print(index)
# =====================================================

# Kortejin uzunluğunu print edin
# =====================================================
tuplex = tuple("TECH ACADEMY")
print(len(tuplex))
# =====================================================

# Siyahını individual kortejlərə parçalayın
# =====================================================
l = [(1,2), (3,4), (8,9)]
print((zip(*l)))
# =====================================================

# Korteji reversed funksiyasının köməkliyi ilə tərsinə çevirin
# =====================================================
x = ("TECH ACADEMY")
y = reversed(x)
print(tuple(y))
# =====================================================

# Sətir formatlanmasının köməkliyi ilə korteji print edin
# Nümunə tuple : (100, 200, 300)
# Nəticə : This is a tuple (100, 200, 300)
# =====================================================
t = (100, 200, 300)
print('This is a tuple {0}'.format(t))
# =====================================================

# Siyahıdakı kortejlərin axırıncı elementini 100 ilə əvəz edin
# Nümunə list: [(10, 20, 40), (40, 50, 60), (70, 80, 90)]
# Nəticə: [(10, 20, 100), (40, 50, 100), (70, 80, 100)]
# =====================================================
l = [(10, 20, 40), (40, 50, 60), (70, 80, 90)]
print([t[:-1] + (100,) for t in l])
# =====================================================

# Siyahıda boş olmayan kortejləri yeni siyahıya yığın və print edin
# Sample data: [(), (), ('',), ('a', 'b'), ('a', 'b', 'c'), ('d')]
# Expected output: [('',), ('a', 'b'), ('a', 'b', 'c'), 'd']
# =====================================================
L = [(), (), ('',), ('a', 'b'), ('a', 'b', 'c'), ('d')]
L = [t for t in L if t]
print(L)
# =====================================================

# Siyahıya qədər olan elementlərin sayını print edin
# =====================================================
num = [10,20,30,(10,20),40]
ctr = 0
for n in num:
    if isinstance(n, tuple):
        break
    ctr += 1
print(ctr)
# =====================================================

# Kortejin tam olaraq nüsxəsini çıxarın
# =====================================================
tuplex = ("HELLO", 5, [], True) 
from copy import deepcopy
print(tuplex)
tuplex_clone = deepcopy(tuplex)
tuplex_clone[2].append(50)
print(tuplex_clone)
print(tuplex)
# =====================================================